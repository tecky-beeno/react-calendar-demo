import React, { useState } from 'react'
import './App.css'
import WeeklyView from './components/calendar/weekly-view'
import { Item } from './components/calendar/weekly-view'
import { DAY, HOUR } from './components/calendar/helpers'

function App() {
  const [i, setI] = useState(1)

  let since = new Date('2021-06-13')
  let items: Item[] = []
  for (let i = 0; i < 14; i++) {
    let startDate = new Date(
      since.getTime() + DAY * Math.floor(Math.random() * 7),
    )
    startDate.setHours(Math.floor(Math.random() * 10 + 8), 30, 0, 0)

    let endDate = new Date(
      startDate.getTime() +
        (HOUR * Math.floor((Math.random() * 4 + 0.5) * 2)) / 2,
    )

    items.push({
      id: i,
      title: '肌肉訓練 ',
      startDate,
      endDate,
    })
  }
  return (
    <WeeklyView
      since={since}
      items={items}
      onEventClick={() => setI(i => i + 1)}
    />
  )
}

export default App
