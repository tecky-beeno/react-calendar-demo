import React from 'react'
import './calendar.css'
import { DAY } from './helpers'

export type Item = {
  id: number
  title: string
  startDate: Date
  endDate: Date
}

type Props = {
  since: Date
  items: Item[]
  onEventClick?: (items: Item) => void
}

const WeekDays = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT']

function range(n: number) {
  return new Array(n).fill(0)
}

function d2(x: number): string {
  return x < 10 ? '0' + x : x.toString()
}

function formatHour(hour: number): string {
  if (!hour) {
    return ''
  }
  return d2(hour) + ':00'
}

function formatTime(date: Date): string {
  let h = date.getHours()
  let m = date.getMinutes()
  return d2(h) + ':' + d2(m)
}

export function WeeklyView(props: Props) {
  // const since = props.since
  const { since } = props

  const sinceTime = since.getTime()

  return (
    <div className="weekly-view">
      <div className="flex week-day-container">
        <div className="timezone">GMT+08</div>
        <div className="w-8" />
        {range(7).map((_, weekDay) => {
          let date = new Date(sinceTime + DAY * weekDay)
          let weekDayText = WeekDays[weekDay]
          return (
            <div className="grow text-center" key={weekDay}>
              <div className="week-day">{weekDayText}</div>
              <div className="date">{date.getDate()}</div>
            </div>
          )
        })}
      </div>
      <div className="flex">
        <div>
          {range(24).map((_, hour) => {
            return (
              <div className="hour-slot hour-label-container" key={hour}>
                <span className="hour-label">{formatHour(hour)}</span>
              </div>
            )
          })}
        </div>
        <div>
          {range(24).map((_, hour) => {
            return <div className="hour-slot hour-line" key={hour} />
          })}
        </div>
        <div className="w-8" aria-hidden>
          ___
        </div>
        {range(7).map((_, weekDay) => {
          return (
            <div className="grow text-center week-slot v-line" key={weekDay}>
              {props.items
                .filter(item => item.startDate.getDay() === weekDay)
                .map(item => {
                  const { title, startDate, endDate } = item

                  let startHour =
                    startDate.getHours() + startDate.getMinutes() / 60
                  let endHour = endDate.getHours() + endDate.getMinutes() / 60
                  let durationHour = endHour - startHour

                  let timeText =
                    durationHour < 1
                      ? ''
                      : durationHour === 1
                      ? formatTime(startDate)
                      : formatTime(startDate) + ' - ' + formatTime(endDate)

                  return (
                    <div
                      className="event-slot"
                      style={{
                        top: `calc(3rem * ${startHour})`,
                        height: `calc(3rem * ${durationHour} - 0.25rem * 2)`,
                      }}
                      key={item.id}
                      onClick={() => props.onEventClick?.(item)}
                    >
                      <b>{title}</b>
                      <div>{timeText}</div>
                    </div>
                  )
                })}
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default WeeklyView
